#!/bin/bash

set -e

cd ${CUNEI_DIR}

if [ "$1" = 'backend' ]; then
  echo "starting cunei-backend..."
  exec dumb-init node dist/backend
fi
if [ "$1" = 'worker' ]; then
  echo "starting cunei-worker..."
  exec dumb-init node dist/worker
fi
