FROM node:13-stretch as builder
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG CUNEI_DIR=/opt/cunei
RUN mkdir -p ${CUNEI_DIR}/
COPY package.json ${CUNEI_DIR}/
COPY src ${CUNEI_DIR}/src
COPY .npmrc ${CUNEI_DIR}/
RUN chown -R node:node ${CUNEI_DIR}/
USER node
WORKDIR ${CUNEI_DIR}
RUN npm install
RUN rm -f .npmrc
RUN npm run build

FROM node:13-stretch
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL="https://gitlab.com/digiresilience/waterbear/cunei"
ARG VERSION

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="redaranj/cunei"
LABEL org.label-schema.description=""
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION
ARG CUNEI_DIR=/opt/cunei
ENV CUNEI_DIR ${CUNEI_DIR}

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    dumb-init
RUN mkdir -p ${CUNEI_DIR}
RUN chown -R node:node ${CUNEI_DIR}/
RUN mkdir -p /var/lib/cunei
RUN chown -R node:node /var/lib/cunei
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
USER node
WORKDIR ${CUNEI_DIR}
COPY --from=builder ${CUNEI_DIR}/node_modules ./node_modules
COPY --from=builder ${CUNEI_DIR}/dist ./dist
EXPOSE 3000
ENV PORT 3000
ENV NODE_ENV production
ENV DATA_PATH /var/lib/cunei
COPY package.json ./
ENTRYPOINT ["/docker-entrypoint.sh"]
