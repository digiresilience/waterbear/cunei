import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';
import { lazy, LazyBoundary } from 'react-imported-component';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  container: {
    padding: 0,
  },
}));

const Basic = lazy(() => import('./Basic.jsx'));
const Consent = lazy(() => import('./Consent.jsx'));
const Loading = lazy(() => import('./Loading.jsx'));
const Login = lazy(() => import('./Login.jsx'));
const Admin = lazy(() => import('./Admin.jsx'));
const Share = lazy(() => import('./Share.jsx'));
const ThankYou = lazy(() => import('./ThankYou.jsx'));

export default function App() {
  const classes = useStyles();
  const history = useHistory();
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const consent = Cookies.get('cunei_consent');
    if (!consent) {
      history.push('/consent');
    }
    setIsLoaded(true);
  }, []);

  if (!isLoaded) {
    return <Loading />;
  } else {
    return (
      <Container className={classes.container}>
        <Switch>
          <LazyBoundary fallback={Loading}>
            <Route exact path="/" render={props => <Basic {...props} />} />
            <Route path="/consent" render={props => <Consent {...props} />} />
            <Route path="/share" render={props => <Share {...props} />} />
            <Route path="/login" render={props => <Login {...props} />} />
            <Route path="/admin" render={props => <Admin {...props} />} />
            <Route path="/thankyou" render={props => <ThankYou {...props} />} />
          </LazyBoundary>
          <Redirect to="/" />
        </Switch>
      </Container>
    );
  }
}
