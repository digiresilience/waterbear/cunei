import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import RoomIcon from '@material-ui/icons/Room';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Box from '@material-ui/core/Box';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Container from '@material-ui/core/Container';
import { FieldsV1 } from '@digiresilience/waterbear-fields';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  h1: {
    fontFamily: 'Poppins',
    fontSize: '26px',
    lineHeight: '32px',
    fontWeight: '700',
  },
  h6: {
    fontFamily: 'Poppins',
    fontSize: '16px',
    lineHeight: '20px',
    fontWeight: '700',
    color: '#4A4A4A',
  },
  sub1: {
    marginTop: theme.spacing(1),
    fontSize: '12px',
    lineHeight: '16px',
    color: '#4A4A4A',
  },
  debug: {
    marginTop: theme.spacing(1),
    fontSize: '12px',
    lineHeight: '16px',
    color: '#4A4A4A',
    fontFamily: 'monospace',
  },
  paper: {
    padding: theme.spacing(4),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  centerText: {
    textAlign: 'center',
  },
}));

export default function Share(props) {
  const [isFormValid, setIsFormValid] = React.useState(true);
  const [openModal, setOpenModal] = React.useState(false);
  const [modalText, setModalText] = React.useState('');
  const [modalDebug, setModalDebug] = React.useState('');
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [geography, setGeography] = useState('');
  const [followUp, setFollowUp] = useState('none');
  const [reason, setReason] = useState(
    FieldsV1.find(f => f.id === 'reason').default,
  );
  const [additionalInfo, setAdditionalInfo] = useState(
    FieldsV1.find(f => f.id === 'additional_info').default,
  );
  const [medium, setMedium] = useState({
    'social-media-second-hand': false,
    'social-media-first-hand': false,
    messaging: false,
    email: false,
    'political-activity': false,
    event: false,
    'online-news': false,
    'video-news': false,
    'audio-news': false,
    commute: false,
  });
  const [mediumOther, setMediumOther] = useState(false);
  const [mediumOtherText, setMediumOtherText] = useState('');
  const classes = useStyles();
  let { files, links, description } = props.location.state;
  useEffect(() => {
    if (props.location.state) {
      if (props.location.state.geography) {
        setGeography(props.location.state.geography);
      }
      if (props.location.state.selectedDate) {
        setSelectedDate(props.location.state.selectedDate);
      }
      if (props.location.state.followUp) {
        setFollowUp(props.location.state.followUp);
      }
      if (props.location.state.reason) {
        setReason(props.location.state.reason);
      }
      if (props.location.state.additionalInfo) {
        setAdditionalInfo(props.location.state.additionalInfo);
      }
      if (props.location.state.medium) {
        setMedium(props.location.state.medium);
      }
      if (props.location.state.mediumOther) {
        setMediumOther(props.location.state.mediumOther);
      }
      if (props.location.state.mediumOtherText) {
        setMediumOtherText(props.location.state.mediumOtherText);
      }
    }
  }, []);

  const handleDateChange = date => {
    setIsFormValid(true);
    setSelectedDate(date);
  };

  //console.log('passed info',props.location.state)

  const handleGeographyChange = event => {
    setGeography(event.target.value);
  };

  const handleFollowUpChange = event => {
    setFollowUp(event.target.value);
  };

  const handleReasonChange = event => {
    setReason(event.target.value);
  };

  const handleAdditionalInfoChange = event => {
    setAdditionalInfo(event.target.value);
  };

  const handleMediumChange = name => event => {
    console.log(name, event.target.checked);
    setMedium({ ...medium, [name]: event.target.checked });
  };

  const getMediumDefault = name => {
    return medium[name];
  };

  const handleMediumOtherChange = event => {
    setMediumOther(event.target.checked);
  };

  const handleMediumOtherTextChange = event => {
    setMediumOtherText(event.target.value);
  };

  const processError = errorMessage => {
    let text = '';
    let debug = '';
    text = `We're sorry your, request didn't go through. Please send the message below to the support team and we'll try to fix things as soon as we can.`;
    debug = JSON.stringify(errorMessage);
    return [text, debug];
  };

  const uploadForm = () => {
    console.log(
      files,
      links,
      description,
      selectedDate,
      geography,
      followUp,
      medium,
      mediumOtherText,
      reason,
      additionalInfo,
    );

    let formData = new FormData();

    files.map((file, i) => {
      formData.append(`file${i}`, file);
      return file;
    });

    formData.append(
      'disinfo_links',
      JSON.stringify(links.filter(link => link.length > 0)),
    );
    formData.append('description', description);
    formData.append('sighted_on', selectedDate);
    formData.append('geography', geography);
    formData.append('follow_up', followUp);

    // make array of true items in object
    const mediumArray = Object.keys(
      Object.keys(medium).reduce((acc, c) => {
        if (medium[c]) acc[c] = medium[c];
        return acc;
      }, {}),
    );

    formData.append('medium', JSON.stringify(mediumArray));
    formData.append('medium_other', mediumOtherText);
    formData.append('reason', reason);
    formData.append('additional_info', additionalInfo);

    let status;
    fetch('/api/v1/reports', {
      method: 'POST',
      body: formData,
    })
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(data => {
        if (status === 200 || status === 201) {
          props.history.push('/thankyou');
          return data;
        } else {
          let [text, debug] = processError(data);
          setModalText(text);
          setModalDebug(debug);
          setOpenModal(true);
          throw new Error(`Error in response from server.`);
        }
      })
      .catch(error => {
        console.error('error:', error);
        throw Error(error.statusText);
      });
  };

  return (
    <Container maxWidth="sm">
      <Paper className={classes.paper} elevation={0}>
        <IconButton
          color="primary"
          component={RouterLink}
          to={{
            pathname: '/',
            state: {
              description,
              files,
              links,
              selectedDate,
              geography,
              followUp,
              reason,
              additionalInfo,
              medium,
              mediumOther,
              mediumOtherText,
            },
          }}
        >
          <ArrowBackIcon />
        </IconButton>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            When did it happen?
          </Typography>
          <FormControl fullWidth>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="MM/dd/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Enter date"
                value={selectedDate}
                onChange={handleDateChange}
                onError={error => {
                  if (error) {
                    setIsFormValid(false);
                  }
                }}
                maxDate={new Date()}
                maxDateMessage={<p>Date cannot be in the future.</p>}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
                InputAdornmentProps={{
                  position: 'start',
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </Box>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            Where were you when it happened?
          </Typography>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item xs={2}>
              <RoomIcon />
            </Grid>
            <Grid item xs={10}>
              <FormControl fullWidth>
                <InputLabel htmlFor="age-native-simple">
                  Select State
                </InputLabel>
                <Select
                  native
                  value={geography}
                  onChange={handleGeographyChange}
                  inputProps={{
                    name: 'geography',
                    id: 'geography',
                  }}
                >
                  <option value="" />
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            What&#39;s suspicious about it?
          </Typography>
          <TextField
            id="reason"
            label="Explain"
            value={reason}
            onChange={handleReasonChange}
            multiline
            rowsMax="4"
            fullWidth
          />
        </Box>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            How did you come across it?
          </Typography>
          <Box mb={3}>
            <Typography
              className={classes.sub1}
              variant="subtitle1"
              component="p"
              gutterBottom
            >
              Check all that apply.
            </Typography>
          </Box>
          <FormControl component="fieldset">
            <FormGroup>
              <FormControlLabel
                onChange={handleMediumChange('social-media-first-hand')}
                value="social-media-first-hand"
                control={<Checkbox color="primary" />}
                label="In my own social media feed from a person or account I follow"
                checked={getMediumDefault('social-media-first-hand')}
              />
              <FormControlLabel
                onChange={handleMediumChange('social-media-second-hand')}
                value="social-media-second-hand"
                control={<Checkbox color="primary" />}
                label="Someone sent it to me on social media"
                checked={getMediumDefault('social-media-second-hand')}
              />
              <FormControlLabel
                onChange={handleMediumChange('messaging')}
                value="messaging"
                control={<Checkbox color="primary" />}
                label="Someone sent it to me via text or another messaging app like WhatsApp or WeChat"
                checked={getMediumDefault('messaging')}
              />
              <FormControlLabel
                onChange={handleMediumChange('email')}
                value="email"
                control={<Checkbox color="primary" />}
                label="Checking my email"
                checked={getMediumDefault('email')}
              />
              <FormControlLabel
                onChange={handleMediumChange('political-activity')}
                value="political-activity"
                control={<Checkbox color="primary" />}
                label="Heard it while engaging in political activity (organizing, door-knocking, volunteering, community organizing)"
                checked={getMediumDefault('political-activity')}
              />
              <FormControlLabel
                onChange={handleMediumChange('event')}
                value="event"
                control={<Checkbox color="primary" />}
                label="At a community event (church, school events, fairs)"
                checked={getMediumDefault('event')}
              />
              <FormControlLabel
                onChange={handleMediumChange('online-news')}
                value="online-news"
                control={<Checkbox color="primary" />}
                label="Checking news online"
                checked={getMediumDefault('online-news')}
              />
              <FormControlLabel
                onChange={handleMediumChange('video-news')}
                value="video-news"
                control={<Checkbox color="primary" />}
                label="Watching the news"
                checked={getMediumDefault('video-news')}
              />
              <FormControlLabel
                onChange={handleMediumChange('audio-news')}
                value="audio-news"
                control={<Checkbox color="primary" />}
                label="Listening to the news"
                checked={getMediumDefault('audio-news')}
              />
              <FormControlLabel
                onChange={handleMediumChange('commute')}
                value="commute"
                control={<Checkbox color="primary" />}
                label="On my commute"
                checked={getMediumDefault('commute')}
              />
              <FormControlLabel
                onChange={handleMediumOtherChange}
                value="other"
                control={<Checkbox color="primary" />}
                label="Other"
                checked={getMediumDefault('other')}
              />
              <TextField
                disabled={!mediumOther}
                onChange={handleMediumOtherTextChange}
                value={mediumOtherText}
              />
            </FormGroup>
          </FormControl>
        </Box>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            Do you have advanced information?
          </Typography>
          <Typography
            className={classes.sub1}
            variant="subtitle1"
            component="p"
          >
            For example: Social media account ID numbers or anything else to
            add.
          </Typography>
          <TextField
            id="standard-multiline-static"
            label="Please share"
            value={additionalInfo}
            onChange={handleAdditionalInfoChange}
            multiline
            rowsMax="4"
            fullWidth
          />
        </Box>
        <Box mt={2} mb={6}>
          <Typography className={classes.h6} variant="h6">
            Would you like someone to follow up with you?
          </Typography>
          <FormControl component="fieldset">
            <RadioGroup value={followUp} onChange={handleFollowUpChange}>
              <FormControlLabel
                value="urgent"
                control={<Radio color="primary" />}
                label="Yes, need immediate followup"
              />
              <FormControlLabel
                value="not-urgent"
                control={<Radio color="primary" />}
                label="Yes, but not urgent"
              />
              <FormControlLabel
                value="none"
                control={<Radio color="primary" />}
                label="None needed"
              />
            </RadioGroup>
          </FormControl>
        </Box>
        <Box mt={2} mb={6}>
          <Grid container direction="row" alignItems="center" justify="center">
            <Grid className={classes.centerText} item xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={uploadForm}
                disabled={!isFormValid}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </Box>
        <Dialog open={openModal} aria-describedby="alert-dialog-description">
          <DialogContent>
            <Box p={2}>
              <DialogContentText id="alert-dialog-description">
                {modalText}
              </DialogContentText>
              <Typography className={classes.debug} component="div">
                {modalDebug}
              </Typography>
            </Box>
          </DialogContent>
        </Dialog>
      </Paper>
    </Container>
  );
}

Share.propTypes = {
  history: PropTypes.object,
  location: PropTypes.shape({
    state: PropTypes.shape({
      description: PropTypes.string,
      files: PropTypes.array,
      links: PropTypes.array,
      geography: PropTypes.string,
      selectedDate: PropTypes.string,
      followUp: PropTypes.string,
      reason: PropTypes.string,
      additionalInfo: PropTypes.string,
      medium: PropTypes.array,
      mediumOther: PropTypes.bool,
      mediumOtherText: PropTypes.string,
    }),
  }),
};
