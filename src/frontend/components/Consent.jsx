// base imports
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Cookies from 'js-cookie';
import { makeStyles } from '@material-ui/core/styles';

// material-ui imports
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Link from '@material-ui/core/Link';
import Modal from '@material-ui/core/Modal';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';

// module imports
import Loading from './Loading.jsx';

// asset imports
import check from '../assets/images/check.png';
import disinfoImg from '../assets/images/disinfo.svg';

const useStyles = makeStyles(theme => ({
  accepted: {
    backgroundColor: '#fff',
    borderRadius: '23px',
    left: '50%',
    padding: '40px',
    position: 'absolute',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  },
  body: {
    color: '#4A4A4A',
    fontSize: '1rem',
    lineHeight: '20px',
  },
  box: {
    textAlign: 'center',
  },
  button: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '30px',
    width: '200px',
  },
  cancel: {
    backgroundColor: '#F44336',
    borderRadius: '23px 23px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '0',
    marginLeft: 'unset',
    marginRight: 'unset',
    marginTop: '20px',
    padding: '11px 22px 8px',
    width: '100%',
    '& .MuiTypography-root.MuiFormControlLabel-label.MuiTypography-body1': {
      color: '#fff',
      fontFamily: 'Poppins',
      fontSize: '1.65rem',
      fontWeight: '700',
      lineHeight: '32px',
      textTransform: 'uppercase',
    },
  },
  cancelBox: {
    backgroundColor: 'rgba(244,67,54,0.24)',
    padding: '22px',
  },
  center: {
    textAlign: 'center',
  },
  container: {
    padding: theme.spacing(4),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  continue: {
    backgroundColor: '#399D6C',
    borderRadius: '23px 23px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '0',
    marginLeft: 'unset',
    marginRight: 'unset',
    padding: '11px 22px 8px',
    width: '100%',
    '& .MuiTypography-root.MuiFormControlLabel-label.MuiTypography-body1': {
      color: '#fff',
      fontFamily: 'Poppins',
      fontSize: '1.65rem',
      fontWeight: '700',
      lineHeight: '32px',
      textTransform: 'uppercase',
    },
  },
  continueBox: {
    backgroundColor: 'rgba(57,157,108,0.26)',
    padding: '22px',
  },
  denied: {
    backgroundColor: '#fff',
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  deniedContent: {
    left: '50%',
    position: 'absolute',
    textAlign: 'center',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  },
  flex: {
    alignItems: 'flex-start',
    display: 'flex',
  },
  h1: {
    fontFamily: 'Poppins',
    fontSize: '1.65rem',
    fontWeight: '700',
    lineHeight: '32px',
    textTransform: 'uppercase',
  },
  h2: {
    color: '#4A4A4A',
    fontFamily: 'Poppins',
    fontSize: '1.25rem',
    fontWeight: '700',
    lineHeight: '28px',
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$radio.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundImage: `url(${check})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    height: '18px',
    width: '20px',
  },
  imgContainer: {
    marginBottom: '30px',
    marginTop: '30px',
  },
  m3: {
    marginBottom: '30px',
    marginTop: '30px',
  },
  m3t: {
    marginTop: '30px',
  },
  poppins: {
    fontFamily: 'Poppins',
    fontSize: '1rem',
    fontWeight: '700',
    lineHeight: '20px',
  },
  radio: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  relative: {
    position: 'relative',
  },
  slogan: {
    fontFamily: 'Poppins',
    fontSize: '1.9rem',
    fontWeight: '700',
    lineHeight: '36px',
  },
  subtitle: {
    color: '#4A4A4A',
    fontFamily: 'Poppins',
    fontSize: '0.8rem',
    fontWeight: '700',
    lineHeight: '16px',
  },
  titleContainer: {
    margin: '30px',
  },
  ul: {
    marginBottom: '16px',
    marginTop: '16px',
    '& li': {
      marginBottom: '16px',
    },
  },
}));

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.radio}
      disableRipple
      color="default"
      checkedIcon={<span className={classes.checkedIcon} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

export default function Consent() {
  const classes = useStyles();
  const history = useHistory();
  const [next, setNext] = useState(false);
  const [consent, setConsent] = useState('');
  const [open, setOpen] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [error, setError] = useState(false);
  const [helperText, setHelperText] = useState('');
  const [buttonText, setButtonText] = useState('Make a choice');

  const handleConsent = event => {
    setConsent(event.target.value);

    if (event.target.value === 'yes') {
      setDisabled(false);
      setButtonText('Confirm');
    } else if (event.target.value === 'no') {
      setDisabled(false);
      setButtonText('Confirm');
    } else {
      setDisabled(true);
      setButtonText('Make a choice');
    }
  };

  const handleConsentAccepted = event => {
    if (event.target.innerHTML === 'Yes') {
      Cookies.set('cunei_consent', true); // cookie does not expire
    } else if (event.target.innerHTML === 'No') {
      Cookies.set('cunei_consent', true, { expires: 0.04 }); // expire cookie in 1 hour
    }

    history.push('/');
  };

  const handleNext = () => {
    setNext(prev => !prev);
  };

  const handleOpen = () => {
    setOpen(true);

    if (consent === 'yes') {
      setError(false);
    } else if (consent === 'no') {
      setError(false);
    } else {
      setHelperText('Please select an option.');
      setError(true);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const denied = (
    <Box className={classes.denied}>
      <Box classname={classes.relative}>
        <Box className={classes.titleContainer}>
          <Typography
            className={classes.h1}
            color="primary"
            variant="h4"
            component="h1"
          >
            Disinfo
          </Typography>
          <Typography className={classes.subtitle}>
            Initiative of Luminate
          </Typography>
        </Box>
        <Box className={classes.deniedContent}>
          <Typography
            className={classes.slogan}
            color="primary"
            variant="h4"
            component="p"
            gutterBottom
          >
            You have chosen not to continue.
          </Typography>
          <Typography
            className={classes.body}
            color="primary"
            variant="body1"
            component="p"
            gutterBottom
          >
            At this time, we are only accepting submissions through this form.
          </Typography>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            href="/consent"
          >
            Start Over
          </Button>
        </Box>
      </Box>
    </Box>
  );

  const accepted = (
    <Box className={classes.accepted}>
      <Box>
        <Typography
          className={classes.slogan}
          color="primary"
          variant="h4"
          component="p"
          gutterBottom
        >
          Remember your choice?
        </Typography>
        <Typography
          className={classes.body}
          color="primary"
          variant="body1"
          component="p"
          gutterBottom
        >
          If you don&lsquo;t want to be asked for permission again. We&lsquo;ll store your choice using cookies.
        </Typography>
        <Grid container className={classes.m3t}>
          <Grid item xs={6} className={classes.center}>
            <Button
              variant="outlined"
              color="primary"
              onClick={event => handleConsentAccepted(event)}
            >
              No
            </Button>
          </Grid>
          <Grid item xs={6} className={classes.center}>
            <Button
              variant="outlined"
              color="primary"
              onClick={event => handleConsentAccepted(event)}
              data-canary="consent-remember"
            >
              Yes
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );

  return (
    <Container maxWidth="sm" className={classes.container}>
      <Typography
        className={classes.h1}
        color="primary"
        variant="h4"
        component="h1"
      >
        Disinfo
      </Typography>
      <Typography className={classes.subtitle}>
        Initiative of Luminate
      </Typography>
      <Box>
        {!next ? (
          <Slide
            direction="right"
            in={!next}
            mountOnEnter
            unmountOnExit
            timeout={{ enter: 0, exit: !next ? 1 : 500 }}
          >
            <Box className={classes.box}>
              <Box className={classes.imgContainer}>
                <img src={disinfoImg} aria-hidden="true" alt="" />
              </Box>
              <Box>
                <Typography
                  className={classes.slogan}
                  color="primary"
                  variant="h4"
                  component="p"
                  gutterBottom
                >
                  Help debunk lies.
                  <br />
                  Empower truth.
                </Typography>
                <Typography
                  className={classes.body}
                  color="primary"
                  variant="body1"
                  component="p"
                  gutterBottom
                >
                  Disinfo crowdsources potentially false information to help fight back against disinformation.
                </Typography>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={handleNext}
                  data-canary="consent-start"
                >
                  Continue
                </Button>
              </Box>
            </Box>
          </Slide>
        ) : null}
        {next ? (
          <Slide
            direction="left"
            in={next}
            mountOnEnter
            unmountOnExit
            timeout={{ enter: 500, exit: next ? 1 : 500 }}
          >
            <Box className={classes.m3}>
              <Typography
                className={classes.h2}
                color="primary"
                variant="h4"
                component="h2"
              >
                Consent
              </Typography>
              <Typography
                className={classes.body}
                color="primary"
                variant="body1"
                component="p"
                gutterBottom
              >
                Disinfo is a tool for documenting cases of mis and
                disinformation. Every submission helps, but it’s important to
                understand the implications of your use of this tool and how
                your contributions are stored and used:
              </Typography>
              <Typography
                className={clsx(classes.body, classes.ul)}
                color="primary"
                variant="body1"
                component="ul"
              >
                <li>
                  This tool’s operator,{' '}
                  <a href="https://luminategroup.com/" target="_blank">Luminate</a>, will
                  collect your email address to allow you to use it.
                </li>
                <li>Luminate stores your submission for later analysis.</li>
                <li>
                  Luminate DOES NOT share any data with advertisers, social media companies, or analytics partners.
                </li>
                <li>
                  <a href="https://www.cloudflare.com" target="_blank">Cloudflare</a> protects
                  this tool from online attacks and will collect your IP address
                  to help accomplish this.
                </li>
                <li>
                  Contributions are stored on secure servers maintained by the
                  Center for Digital Resilience (CDR).{' '}
                  <a href="https://docs.digiresilience.org/about/cdr-privacy-policy/" target="_blank">
                    Read the CDR Privacy Policy
                  </a>
                  .
                </li>
              </Typography>
              <Box className={classes.m3}>
                <Typography
                  className={classes.body}
                  color="primary"
                  variant="body1"
                  component="p"
                  gutterBottom
                >
                  <Link href="https://docs.digiresilience.org/about/cdr-privacy-policy/" target="_blank">
                    CDR Privacy Policy
                  </Link>
                </Typography>
                <Typography
                  className={classes.body}
                  color="primary"
                  variant="body1"
                  component="p"
                  gutterBottom
                >
                  <Link href="https://luminategroup.com/about" target="_blank">About Luminate</Link>
                </Typography>
                <Typography
                  className={classes.body}
                  color="primary"
                  variant="body1"
                  component="p"
                  gutterBottom
                >
                  <Link href="https://www.cloudflare.com/privacypolicy/" target="_blank">
                    About Cloudflare
                  </Link>
                </Typography>
              </Box>
              <form>
                <FormControl component="fieldset" error={error}>
                  <Hidden xsUp>
                    <FormLabel component="legend">Continue</FormLabel>
                  </Hidden>
                  <RadioGroup
                    aria-label="consent"
                    name="consent"
                    value={consent}
                    onChange={handleConsent}
                    required
                  >
                    <Box>
                      <FormControlLabel
                        className={classes.continue}
                        value="yes"
                        control={<StyledRadio />}
                        label="Continue"
                        labelPlacement="start"
                        data-canary="consent-approve"
                      />
                      <Box className={classes.continueBox}>
                        <Typography
                          className={clsx(classes.body, classes.poppins)}
                          color="primary"
                          variant="body1"
                          component="p"
                          gutterBottom
                        >
                          Continue if
                        </Typography>
                        <Typography
                          className={classes.body}
                          color="primary"
                          variant="body1"
                          component="ul"
                          gutterBottom
                        >
                          <li>
                            Sharing your IP and email address with this tool is unlikely to put
                            you in danger.
                          </li>
                          <li>
                            You understand that your submissions will be made available to
                            researchers as part of an effort to understand disinformation.
                          </li>
                          <li>
                            You understand that your submissions are stored in accordance with{' '}
                            <a
                              href="https://oag.ca.gov/privacy/ccpa"
                              target="_blank"
                            >
                              California law.
                            </a>
                          </li>
                        </Typography>
                      </Box>
                    </Box>
                    <Box>
                      <FormControlLabel
                        className={classes.cancel}
                        value="no"
                        control={<StyledRadio />}
                        label="Cancel"
                        labelPlacement="start"
                        data-canary="consent-reject"
                      />
                      <Box className={classes.cancelBox}>
                        <Typography
                          className={clsx(classes.body, classes.poppins)}
                          color="primary"
                          variant="body1"
                          component="p"
                          gutterBottom
                        >
                          Do not continue if
                        </Typography>
                        <Typography
                          className={classes.body}
                          color="primary"
                          variant="body1"
                          component="ul"
                          gutterBottom
                        >
                          <li>
                            Your use of this tool is likely to endanger you or someone you know.
                          </li>
                          <li>
                            You are unsure of the privacy implications of documenting and
                            submitting instances of disinformation with this tool.
                          </li>
                        </Typography>
                      </Box>
                    </Box>
                  </RadioGroup>
                  <FormHelperText>{helperText}</FormHelperText>
                  <Button
                    className={classes.button}
                    onClick={handleOpen}
                    variant="contained"
                    color="primary"
                    disabled={disabled}
                    data-canary="consent-confirm"
                  >
                    {buttonText}
                  </Button>
                  <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                  >
                    {consent === 'yes' ? accepted : denied}
                  </Modal>
                </FormControl>
              </form>
            </Box>
          </Slide>
        ) : null}
      </Box>
    </Container>
  );
}
