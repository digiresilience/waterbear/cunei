import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import MaterialTable from 'material-table';

import Search from '@material-ui/icons/Search';
import SaveAlt from '@material-ui/icons/SaveAlt';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Add from '@material-ui/icons/Add';
import Check from '@material-ui/icons/Check';
import Clear from '@material-ui/icons/Clear';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import Refresh from '@material-ui/icons/Refresh';
import FilterList from '@material-ui/icons/FilterList';
import Remove from '@material-ui/icons/Remove';
import { fetchWithTimeout } from '../../common/utils.js';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

export default function ReportTable() {
  const classes = useStyles();
  const tableRef = React.createRef();
  const TIMEOUT = 1000;

  function refreshIcon() {
    return <Refresh />;
  }

  async function fetchJobs(page, pageSize) {
    const start = page * pageSize;
    const end = start + pageSize;
    let url = 'api/v1/reports?';
    url += 'start=' + start;
    url += '&end=' + end;
    return fetchWithTimeout(url, {}, TIMEOUT)
      .then(response => response.json())
      .then(result => {
        console.log('result.data: ', result.data);
        return {
          data: result.jobs,
          page: page,
          totalCount: result.total,
        };
      });
  }

  return (
    <Paper className={classes.paper}>
      <MaterialTable
        title="Reports in Queue"
        tableRef={tableRef}
        icons={{
          Add: Add,
          Clear: Clear,
          Check: Check,
          Delete: Delete,
          Edit: Edit,
          DetailPanel: ChevronRight,
          Export: SaveAlt,
          Filter: FilterList,
          FirstPage: FirstPage,
          LastPage: LastPage,
          NextPage: ChevronRight,
          PreviousPage: ChevronLeft,
          Refresh: Refresh,
          Search: Search,
          ThirdStateCheck: Remove,
        }}
        columns={[
          { title: 'Queue', field: 'queue' },
          { title: 'ID', field: 'id' },
          { title: 'Title', field: 'title' },
          { title: 'Submitter', field: 'submitter' },
          { title: 'Status', field: 'status' },
          { title: 'Attempts', field: 'attemptsMade' },
          { title: 'Failure Reason', field: 'failedReason' },
        ]}
        data={query => fetchJobs(query.page, query.pageSize)}
        actions={[
          {
            icon: refreshIcon,
            tooltip: 'Refresh Data',
            isFreeAction: true,
            onClick: () => tableRef.current && tableRef.current.onQueryChange(),
          },
        ]}
        options={{
          search: false,
          sorting: false,
        }}
      />
    </Paper>
  );
}
