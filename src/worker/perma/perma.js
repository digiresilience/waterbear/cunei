//
// Send url submitted with form to Perma.cc
//

'use strict';

import fetch from 'node-fetch';
import config from '../config.js';

async function postData(permaUrl = '', apiKey, data = {}) {
  console.debug(`Posting URL ${data.url} to Perma ${permaUrl}`);
  const response = await fetch(permaUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Token ${apiKey}`,
    },
    body: JSON.stringify(data),
  });
  return response.json(); // parses JSON response into native JavaScript objects
}
/* eslint-disable no-unused-vars */
export const saveToPerma = async (id, urls) => {
  /* eslint-enable no-unused-vars */

  if (
    urls &&
    urls.length > 0 &&
    config.perma_api_key &&
    config.perma_api &&
    config.zammad_url
  ) {
    let instance = config.zammad_url
      .replace('https://', '')
      .split('.')
      .shift();

    postData(config.perma_api, config.perma_api_key, {
      urls,
      tag: `${instance}-${id}`,
    })
      .then(data => {
        console.debug('Posted URL to Perma: ', data);
        return data;
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }
  return null;
};
