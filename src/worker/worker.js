import { Worker } from 'bullmq';
import log4js from 'koa-log4';
import Koa from 'koa';
import prometheus from '@guardianproject-ops/koa-prometheus-exporter';
import { createTicket } from './zammad/ticket.js';
import { saveToArchive } from './archive/archive.js';
import { saveToPerma } from './perma/perma.js';

export function startWorker(config) {
  // Configure logging
  log4js.configure({
    appenders: { console: { type: 'stdout', layout: { type: 'colored' } } },
    categories: {
      default: { appenders: ['console'], level: config.log_level },
    },
  });

  const log = log4js.getLogger('worker:worker');

  const submissionCounter = new prometheus.client.Counter({
    name: 'cunei_submissions_total',
    help: 'The total number of submissions',
  });
  const failureCounter = new prometheus.client.Counter({
    name: 'cunei_failed_submissions_total',
    help: 'The total number of submissions that failed to submit',
  });

  const job = async job => {
    log.debug('Worker processed job: ', job.data);
    let zammadData;
    let archiveData;
    let permaData;
    submissionCounter.inc();
    try {
      zammadData = await createTicket(job.data);
      archiveData = await saveToArchive(job.data.disinfo_links);
      permaData = await saveToPerma(
        zammadData.ticket.number,
        job.data.disinfo_links,
      );
    } catch (err) {
      log.error(`Failed to create ticket: ${err}`);
      failureCounter.inc();
    }
    log.debug('Created ticket in Zammad: ', zammadData);
    log.debug('Sent urls to Internet Archive: ', archiveData);
    log.debug('Sent urls to Perma: ', permaData);
  };

  const worker = new Worker(config.queue, job, {
    connection: { host: config.redis_host, port: config.redis_port },
  });
  return worker;
}

export function configMetrics() {
  const metricsApp = new Koa();
  metricsApp.use(prometheus.middleware());
  return metricsApp.callback();
}
