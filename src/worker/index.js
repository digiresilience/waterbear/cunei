#!/usr/bin/env node
import config from './config.js';
import { createServer } from 'http';
config.parse(process.argv);

import { startWorker, configMetrics } from './worker.js';

async function bootstrap() {
  if (!config.zammad_url) {
    throw 'No Zammad endpoint defined, please use CUNEI_ZAMMAD_URL environment variable.';
  }
  if (!config.zammad_token) {
    throw 'No Zammad token defined, please use CUNEI_ZAMMAD_TOKEN environment variable.';
  }
  const metrics = await createServer(configMetrics()).listen(
    config.metrics_port,
  );
  const worker = await startWorker(config);
  return { metrics, worker };
}

bootstrap()
  .then(({ metrics }) =>
    console.log(
      `👷 Worker operating over queue ${config.queue} on ${config.redis_host}:${
        config.redis_port
      }!
   Metrics listening on port ${metrics.address().port}!
`,
    ),
  )
  .catch(err => {
    setImmediate(() => {
      console.error('Unable to run worker because of the following error:');
      console.error(err);
    });
  });
