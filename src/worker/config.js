import { Command } from 'commander';
import Joi from '@hapi/joi';
import dotenv from 'dotenv';

/**
 * Optionally load environment from a .env file.
 */

dotenv.config();

const defaults = {
  loglevel: process.env.CUNEI_LOG_LEVEL || 'error',
  archive: {
    api: process.env.CUNEI_WAYBACK_API || 'https://web.archive.org/save',
    accessKey: process.env.CUNEI_WAYBACK_ACCESS_KEY || '',
    secretKey: process.env.CUNEI_WAYBACK_SECRET_KEY || '',
  },
  perma: {
    api: process.env.CUNEI_PERMA_API || 'https://capture.perma.cc/api/captures',
    apiKey: process.env.CUNEI_PERMA_API_KEY || '',
  },
  cfaccess: {
    clientId: process.env.CUNEI_CFACCESS_CLIENT_ID || '',
    clientSecret: process.env.CUNEI_CFACCESS_CLIENT_SECRET || '',
  },
  redis: {
    host: process.env.CUNEI_REDIS_HOST || 'localhost',
    port: process.env.CUNEI_REDIS_PORT || 6379,
  },
  worker: {
    queue: process.env.CUNEI_WORKER_QUEUE || '0',
  },
  zammad: {
    group: process.env.CUNEI_ZAMMAD_GROUP || 'Users',
    url: process.env.CUNEI_ZAMMAD_URL || '',
    token: process.env.CUNEI_ZAMMAD_TOKEN || '',
  },
  cloudflare: {
    clientId: process.env.CUNEI_CFACCESS_CLIENT_ID || '',
    clientSecret: process.env.CUNEI_CFACCESS_CLIENT_SECRET || '',
  },
  metrics: {
    port: process.env.CUNEI_METRICS_PORT || '9396',
  },
};

function validateUrl(value, previous) {
  const url = value ? value : previous;
  Joi.assert(url, Joi.string().uri());
  return url;
}

function validateToken(value, previous) {
  const token = value ? value : previous;
  Joi.assert(token, Joi.string().required());
  return token;
}

function validateLoglevel(value, previous) {
  const level = value ? value : previous;
  Joi.assert(
    level,
    Joi.string()
      .allow('trace', 'debug', 'info', 'warn', 'error', 'fatal')
      .required(),
  );
  return level;
}

function validateHost(value, previous) {
  const host = value ? value : previous;
  Joi.assert(host, Joi.string().required());
  return host;
}

function validatePort(value, previous) {
  const port = value ? value : previous;
  Joi.assert(port, Joi.number().required());
  return port;
}

function validateQueueId(value, previous) {
  const id = value ? value : previous;
  Joi.assert(id, Joi.string().required());
  return id;
}

class Config extends Command {
  constructor(...args) {
    super(args);
    const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
    Joi.string()
      .allow('development', 'production', 'test')
      .required()
      .validate(env);
    this.isDev = env === 'development';
    this.isTest = env === 'test';
    this.isProd = env === 'production';
  }
}

const program = new Config();

export default program
  .description(process.env.npm_package_description)
  .version(process.env.npm_package_version)
  .option(
    '-l, --log_level <level>',
    'Logging verbosity',
    validateLoglevel,
    defaults.loglevel,
  )
  .option(
    '-q, --queue <name>',
    'Default worker queue ID',
    validateQueueId,
    defaults.worker.queue,
  )
  .option(
    '--redis_host <host>',
    'Redis host',
    validateHost,
    defaults.redis.host,
  )
  .option(
    '--redis_port <port>',
    'Redis port',
    validatePort,
    defaults.redis.port,
  )
  .option(
    '--cfaccess_client_id <id>',
    'Cloudflare Access client ID',
    validateToken,
    defaults.cfaccess.clientId,
  )
  .option(
    '--cfaccess_client_secret <secret>',
    'Cloudflare Access client secret',
    validateToken,
    defaults.cfaccess.clientSecret,
  )
  .option(
    '--zammad_group <group>',
    'Zammad Group',
    validateToken,
    defaults.zammad.group,
  )
  .option('--zammad_url <url>', 'Zammad URL', validateUrl, defaults.zammad.url)
  .option(
    '--zammad_token <token>',
    'Zammad Token',
    validateToken,
    defaults.zammad.token,
  )
  .option(
    '--cloudflare_client_id <clientId>',
    'Cloudflare Client ID',
    validateToken,
    defaults.cloudflare.clientId,
  )
  .option(
    '--cloudflare_client_secret <clientSecret>',
    'Cloudflare Client secret',
    validateToken,
    defaults.cloudflare.clientSecret,
  )
  .option(
    '--archive_api <api>',
    'Archive API',
    validateUrl,
    defaults.archive.api,
  )
  .option(
    '--archive_access_key <accessKey>',
    'Archive Access Key',
    validateToken,
    defaults.archive.accessKey,
  )
  .option(
    '--archive_secret_key <secretKey>',
    'Archive Secret Key',
    validateToken,
    defaults.archive.secretKey,
  )
  .option('--perma_api <api>', 'Perma API', validateUrl, defaults.perma.api)
  .option(
    '--perma_api_key <accessKey>',
    'Perma API Key',
    validateToken,
    defaults.perma.apiKey,
  )
  .option(
    '--metrics_port <number>',
    'Port for prometheus metrics to listen on',
    validatePort,
    defaults.metrics.port,
  );
