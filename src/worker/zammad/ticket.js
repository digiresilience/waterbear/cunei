import { promises as fs } from 'fs';
import { post } from './rest.js';

const formatArticleBody = ticket => {
  if (!ticket.article.attachments) {
    return '<p><strong>No attachments</strong></p>';
  }
  let date = new Date(ticket.sighted_on);
  let fileInfo = '';
  if (ticket.article.attachments) {
    let images = ticket.article.attachments.filter(file =>
      file['mime-type'].includes('image'),
    );
    let otherFiles = ticket.article.attachments.filter(
      file => !file['mime-type'].includes('image'),
    );

    fileInfo += '<p><strong>Images:<strong></p><p>&nbsp;</p>';
    if (images.length > 0) {
      for (const file of images) {
        fileInfo += `<p><img width="200" src="data:${
          file['mime-type']
        };base64,${file.data}"></p><p>&nbsp;</p>`;
      }
    } else {
      fileInfo += `<p>none</p><p>&nbsp;</p>`;
    }
    fileInfo += '<p><strong>Other files:<strong></p><p>&nbsp;</p>';
    if (otherFiles.length > 0) {
      fileInfo += `<ul>${otherFiles.reduce(
        (acc, cur) => `${acc}<li>${cur.filename}</li>\n`,
        '',
      )}</ul>`;
    } else {
      fileInfo += `<p>none</p><p>&nbsp;</p>`;
    }
  }
  let body = `
<p>
  <strong>What I saw:</strong> ${ticket.description}
</p>
<p>&nbsp;</p>

<p>
  <strong>Links:</strong> <ul>${ticket.disinfo_links.reduce(
    (acc, cur) => `${acc}<li>${cur}</li>\n`,
    '',
  )}</ul>
</p>
<p>&nbsp;</p>

<div>
  ${fileInfo}
</div>
<p>&nbsp;</p>

<p>
  <strong>Date:</strong> ${date.toLocaleDateString()}
</p>
<p>&nbsp;</p>

<p>
  <strong>Geography:</strong> ${ticket.geography || ''}
</p>
<p>&nbsp;</p>

<p>
  <strong>What's suspicious:</strong> ${ticket.reason}
</p>
<p>&nbsp;</p>

<p>
  <strong>Medium:</strong> <ul>${ticket.medium.reduce(
    (acc, cur) => `${acc}<li>${cur}</li>\n`,
    '',
  )}
      ${
        ticket.medium_other && ticket.medium_other != ''
          ? `<li>Other: ${ticket.medium_other}</li>\n`
          : ''
      }</ul>
</p>
<p>&nbsp;</p>

<p>
  <strong>Other info:</strong> ${ticket.additional_info}
</p>
<p>&nbsp;</p>

<p>
  <strong>Follow up:</strong> ${ticket.follow_up}
</p>
`;
  // Zammad has a maximum body length, if we go over that we should send a version without inline images
  if (body.length > 1300000) {
    body = `<p>
  <strong>What I saw:</strong> ${ticket.description}
</p>
<p>&nbsp;</p>

<p>
<strong>Links:</strong> <ul>${ticket.disinfo_links.reduce(
      (acc, cur) => `${acc}<li>${cur}</li>\n`,
      '',
    )}</ul>
</p>
<p>&nbsp;</p>

<div>
  <p>(Images are too big to render, please see below for attachments.)</p>
</div>
<p>&nbsp;</p>

<p>
  <strong>Date:</strong> ${date.toLocaleDateString()}
</p>
<p>&nbsp;</p>

<p>
  <strong>Geography:</strong> ${ticket.geography || ''}
</p>
<p>&nbsp;</p>

<p>
  <strong>What's suspicious:</strong> ${ticket.reason}
</p>
<p>&nbsp;</p>

<p>
  <strong>Medium:</strong> <ul>${ticket.medium.reduce(
    (acc, cur) => `${acc}<li>${cur}</li>\n`,
    '',
  )}
${
  ticket.medium_other && ticket.medium_other != ''
    ? `<li>Other: ${ticket.medium_other}</li>\n`
    : ''
}
</ul>
</p>
<p>&nbsp;</p>

<p>
  <strong>Other info:</strong> ${ticket.additional_info}
</p>
<p>&nbsp;</p>

<p>
  <strong>Follow up:</strong> ${ticket.follow_up}
</p>`;
  }
  return body;
};

export const createTicket = async ticket => {
  let shortTitleMaxChars = 50;
  let shortTitle =
    ticket.description.length < shortTitleMaxChars
      ? ticket.description
      : ticket.description.substr(0, shortTitleMaxChars) + '...';
  ticket.article = {};
  ticket.article.content_type = 'text/html';
  ticket.article.subject = shortTitle || 'Web form subject';
  if (ticket.files) {
    ticket.article.attachments = [];
    for (const file of ticket.files) {
      let base64Data = await fs.readFile(file.path, 'base64');
      ticket.article.attachments.push({
        filename: file.name,
        'mime-type': file.type,
        data: base64Data,
      });
    }
  }
  ticket.article.body = formatArticleBody(ticket);
  ticket.title = shortTitle || 'Web form ticket';
  const data = await post('', { body: ticket, json: true });
  return data;
};
