import log4js from 'koa-log4';
import rp from 'request-promise-native';
import config from '../config.js';

const log = log4js.getLogger('worker:zammad:rest');

const globalHeaders = {
  'Content-Type': 'application/json',
  Authorization: `Token token=${config.zammad_token}`,
};

function getGlobalOpts() {
  if (config.cfaccess_client_id && config.cfaccess_client_secret) {
    const cfaccessClientHeaders = {
      'CF-Access-Client-Id': config.cfaccess_client_id,
      'CF-Access-Client-Secret': config.cfaccess_client_secret,
    };
    return { headers: { ...globalHeaders, ...cfaccessClientHeaders } };
  } else {
    return { headers: globalHeaders };
  }
}

export const get = async (path, passedOpts = {}) => {
  let data;
  const uri = `${config.zammad_url}${path}`;
  const globalOpts = getGlobalOpts();
  try {
    data = await rp.get({
      uri: uri,
      ...globalOpts,
      ...passedOpts,
    });
  } catch (err) {
    log.error(`Error GETing from URI ${uri}: ${err}`);
    throw err;
  }
  return data;
};

export const post = async (path, passedOpts = {}) => {
  let data;
  const uri = `${config.zammad_url}${path}`;
  const globalOpts = getGlobalOpts();
  if (passedOpts.body && passedOpts.body.authtoken) {
    log.debug(
      'Setting authorization header to passed token ',
      passedOpts.body.authtoken,
    );
    globalOpts.headers.Authorization = `${passedOpts.body.authtoken}`;
  }
  try {
    data = await rp.post({
      uri: uri,
      ...globalOpts,
      ...passedOpts,
    });
  } catch (err) {
    log.error(`Error POSTing to URI ${uri}: ${err}`);
    throw err;
  }
  return data;
};

export const put = async (path, passedOpts = {}) => {
  let data;
  const uri = `${config.zammad_url}${path}`;
  const globalOpts = getGlobalOpts();
  try {
    data = await rp.put({
      uri: uri,
      ...globalOpts,
      ...passedOpts,
    });
  } catch (err) {
    log.error(`Error PUTing to URI ${uri}: ${err}`);
    throw err;
  }
  return data;
};

export const del = async (path, passedOpts = {}) => {
  let data;
  const uri = `${config.zammad_url}${path}`;
  const globalOpts = getGlobalOpts();
  try {
    data = await rp.del({
      uri: uri,
      ...globalOpts,
      ...passedOpts,
    });
  } catch (err) {
    log.error(`Error DELeting from URI ${uri}: ${err}`);
    throw err;
  }
  return data;
};
