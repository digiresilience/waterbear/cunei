//
// Send url submitted with form to the Internet Archive
//

'use strict';

import fetch from 'node-fetch';
import config from '../config.js';

async function postData(archiveUrl = '', accessKey, secretKey, data = {}) {
  console.debug(`Posting URL ${data.url} to archive ${archiveUrl}`);
  const response = await fetch(archiveUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `LOW ${accessKey}:${secretKey}`,
    },
    body: JSON.stringify(data),
  });
  return response.json(); // parses JSON response into native JavaScript objects
}
/* eslint-disable no-unused-vars */
export const saveToArchive = async urls => {
  /* eslint-enable no-unused-vars */

  const ret = [];
  if (
    config.archive_access_key &&
    config.archive_secret_key &&
    config.archive_api
  ) {
    urls.forEach(url => {
      postData(
        config.archive_api,
        config.archive_access_key,
        config.archive_secret_key,
        {
          url:
            url + '&capture_all=1&capture_outlinks=1&outlinks_availability=1',
        },
      )
        .then(data => {
          console.debug('Posted URL to Wayback Machine: ', data);
          return ret.push(data);
        })
        .catch(error => {
          console.error('Error:', error);
        });
    });
  }
  return ret;
};
