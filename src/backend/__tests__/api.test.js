import RedisServer from 'redis-server';
import request from 'supertest';
import Session from 'supertest-session';
import prometheus from '@guardianproject-ops/koa-prometheus-exporter';
import tcpPortUsed from 'tcp-port-used';

import config from '../config.js';
import { configMetrics, configServer as server } from '../server.js';
import Queue from '../models/queue.js';

const MOCK_SERVER_HOST = 'localhost';
const MOCK_SERVER_PORT = 6379;
var redis;

beforeAll(() => {
  prometheus.client.register.clear();
  tcpPortUsed
    .check(MOCK_SERVER_PORT, MOCK_SERVER_HOST)
    .then(async inUse => {
      if (!inUse) {
        redis = new RedisServer({ port: MOCK_SERVER_PORT });
        return redis.open();
      } else {
        console.log(
          'Using redis server already running on port ',
          MOCK_SERVER_PORT,
        );
        return;
      }
    })
    .catch(err => {
      console.error('Error checking redis port: ', err.message);
    });
});

afterAll(async () => {
  if (redis && redis.isRunning) {
    await redis.close();
  } else {
    console.warn(
      'Unable to shutdown redis manager, maybe redis was already running?',
    );
  }
});

describe('Unauthenticated use of API', () => {
  beforeEach(() => {
    prometheus.client.register.clear();
  });
  test('Add valid report to processing queue.', async () => {
    await request(server(config))
      .post('/api/v1/reports')
      .field({ description: 'I saw a lie.' })
      .field({
        disinfo_links: JSON.stringify([
          'https://facebook.com',
          'https://twitter.com',
        ]),
      })
      .field({ witness: 'me' })
      .field({
        medium: JSON.stringify(['social-media-first-hand', 'video-news']),
      })
      .field({ medium_other: 'Other stuff' })
      .field({ submitter_email: 'foo@example.com' })
      .field({ reason: "It's bad" })
      .field({ additional_info: 'Very secret, very bad.' })
      .field({ geography: 'DC' })
      .expect(201);
  });

  test('Add invalid report to processing queue.', async () => {
    await request(server(config))
      .post('/api/v1/reports')
      .field({ depiction: 'I saw a lie.' })
      .field({
        disinfo_links: JSON.stringify([
          'httpsfacebook.com',
          'httpstwitter.com',
        ]),
      })
      .field({ witness: 'none' })
      .field({ medium: 'pigeon' })
      .field({ medium_other: 'Other stuff' })
      .field({ submitter_email: 'foo@example.com' })
      .field({ reason: "It's bad" })
      .field({ sensitivity: 'Very secret.' })
      .field({ additional_info: 'Very secret, very bad.' })
      .field({ geography: 'blah' })
      .expect(422);
  });

  test('Add valid report to specific processing queue.', async () => {
    await request(server(config))
      .post('/api/v1/queues/test/reports')
      .field({ description: 'I saw a lie.' })
      .field({
        disinfo_links: JSON.stringify([
          'https://facebook.com',
          'https://twitter.com',
        ]),
      })
      .field({ witness: 'me' })
      .field({
        medium: JSON.stringify(['social-media-first-hand', 'video-news']),
      })
      .field({ medium_other: 'Other stuff' })
      .field({ submitter_email: 'foo@example.com' })
      .field({ reason: "It's bad" })
      .field({ sensitivity: 'Very secret.' })
      .field({ additional_info: 'Very secret, very bad.' })
      .field({ geography: 'DC' })
      .expect(201);
  });

  test('Add invalid report to specific processing queue.', async () => {
    await request(server(config))
      .post('/api/v1/queues/test/reports')
      .field({ depiction: 'I saw a lie.' })
      .field({
        disinfo_links: JSON.stringify([
          'httpsfacebook.com',
          'httpstwitter.com',
        ]),
      })
      .field({ witness: 'none' })
      .field({ medium: 'pigeon' })
      .field({ medium_other: 'Other stuff' })
      .field({ submitter_email: 'foo@example.com' })
      .field({ reason: "It's bad" })
      .field({ sensitivity: 'Very secret.' })
      .field({ additional_info: 'Very secret, very bad.' })
      .field({ geography: 'blah' })
      .expect(422);
  });

  test('Try and get reports without authenticating', async () => {
    await request(server(config))
      .get('/api/v1/reports')
      .expect(401);
  });

  test('Try and get reports from specific queue without authenticating', async () => {
    await request(server(config))
      .get('/api/v1/queues/test/reports')
      .expect(401);
  });
});

describe('Authenticate to API', () => {
  let session;
  beforeEach(() => {
    prometheus.client.register.clear();
    session = Session(server(config));
  });

  afterAll(async () => {
    session.destroy();
  });

  test('Authenticate unsuccessfully', async () => {
    await session
      .post('/api/v1/login')
      .send({ username: 'admin', password: 'wrongpassword' })
      .expect(401);
  });

  test('Authenticate successfully', async () => {
    await session
      .post('/api/v1/login')
      .send({ username: config.username, password: config.password })
      .expect(200);
  });
});

describe('Query authenticated API', () => {
  let queue;
  let session;
  const jobs = [
    { name: 'job1', data: 'Test data 1' },
    { name: 'job2', data: 'Test data 2' },
    { name: 'job3', data: 'Test data 3' },
  ];

  beforeAll(async () => {
    prometheus.client.register.clear();
    session = Session(server(config));
    await session
      .post('/api/v1/login')
      .send({ username: config.username, password: config.password })
      .expect(200);
  });

  beforeEach(() => {
    prometheus.client.register.clear();
    queue = new Queue(MOCK_SERVER_HOST, MOCK_SERVER_PORT);
    jobs.map(job => {
      queue.enqueue({ job });
    });
  });

  afterAll(async () => {
    session.destroy();
    await queue.close();
  });

  test('Get all jobs', async () => {
    await session.get('/api/v1/reports').expect(200);
    //const res = await session.get('/api/v1/reports').expect(200);
    //const fetchedJobs = JSON.parse(res.text);
    //expect(fetchedJobs.jobs).toEqual(jobs);
  });
});

describe('Test metrics', () => {
  beforeEach(() => {
    prometheus.client.register.clear();
  });
  test('Get metrics', async () => {
    // configure main server to setup prom metrics
    server(config);
    // configure metrics server
    await request(configMetrics())
      .get('/metrics')
      .expect(res => {
        expect(res.text).toMatch(/http_requests_total/);
      })
      .expect(200);
  });
});
