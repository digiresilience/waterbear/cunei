#!/usr/bin/env node
import figlet from 'figlet';
import { createServer } from 'http';
import config from './config.js';
import { configServer, configMetrics } from './server.js';

/**
 * Function to start up Cunei.
 */
async function bootstrap() {
  /**
   * Add external services init as async operations (db, redis, etc...)
   * e.g.
   * await sequelize.authenticate()
   */
  config.parse(process.argv);
  const app = await createServer(configServer(config)).listen(config.port);

  const metrics = await createServer(configMetrics()).listen(
    config.metrics_port,
  );
  return {
    app,
    metrics,
  };
}

bootstrap()
  .then(async ({ app, metrics }) => {
    figlet.text(
      process.env.npm_package_name,
      {
        font: 'Sub-Zero',
      },
      function(err, bigName) {
        if (err) {
          console.error('Something went wrong...');
          console.error(err);
          return;
        }
        console.log(`
${bigName}
🚀 Server listening on port ${app.address().port}!
   Metrics listening on port ${metrics.address().port}!
`);
        return;
      },
    );
    return;
  })
  .catch(err => {
    setImmediate(() => {
      console.error('Encountered error while running Cunei: ', err);
    });
  });
