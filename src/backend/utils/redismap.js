import Redis from 'ioredis';
import { isString, jsonThing } from '../../common/utils.js';
import { getLogger } from '../log.js';

const log = getLogger('backend:utils:redismap');

export default class RedisMap extends Map {
  constructor({ name, readthrough = false, host = '127.0.0.1', port = 6379 }) {
    super();
    this._readthrough = readthrough;
    this._keyPrefix = name;
    this._redis = new Redis({
      keyPrefix: this._keyPrefix,
      port: port,
      host: host,
    });
    this._redis.Promise = global.Promise;
    this.isLoaded = false;
  }

  async load() {
    const hash = await this._redis.hgetall('hosts');
    Object.entries(hash).forEach(([key, value]) => {
      let val;
      try {
        val = JSON.parse(value);
      } catch (err) {
        log.debug('Could not parse value to object: ', value);
        if (isString(value)) {
          log.debug('Value is already a string: ', value);
          val = value;
        } else {
          throw new Error('Failed to load value from Redis.');
        }
      }
      this.set(key, val);
    });

    log.debug('Loading Map from redis');
    this.isLoaded = true;
    return;
  }

  set(key, value) {
    const s = jsonThing(value);
    this._redis.hset('hosts', key, s);
    super.set(key, value);
  }

  delete(key) {
    this._redis.hdel('hosts', key);
    super.delete(key);
  }
}
