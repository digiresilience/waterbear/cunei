# Cunei(form)

> A standalone webform submission tool. Part of the [CDR Waterbear](https://digiresilience.org/tech/waterbear.html) project.

[![pipeline status](https://gitlab.com/digiresilience/waterbear/cunei/badges/master/pipeline.svg)](https://gitlab.com/digiresilience/waterbear/cunei/-/commits/master)
[![coverage report](https://gitlab.com/digiresilience/waterbear/cunei/badges/master/coverage.svg)](https://gitlab.com/digiresilience/waterbear/cunei/-/commits/master)

## Requirements

* [Node.js](https://nodejs.org) of version `>=10`
* `npm`
* Cunei uses [Redis](https://redis.org) to store its state.

## Structure

Cunei is composed of 3 different parts:

* A [React](https://reactjs.org/)-based **frontend** that provides a standalone webform tool to submit tickets destined for an instance of [Zammad](https://zammad.org).
* A [Koa](https://koajs.com)-based **backend** that renders & serves the frontend and exposes an API used by the frontend. Form submissions sent to this API get entered into a work-queue in an instance of Redis.
* A standalone Javascript **worker** that consumes the work-queue and submits forms to Zammad.

These parts are located here in this repository:
```
src/backend  # The backend components
src/common   # Common code and assets
src/frontend # The React frontend
src/worker   # The standalone worker
```

## Configuration
Cunei is configured via variables either specified in the environment or defined in a `.env` file (see `env.example` for an example configuration that may be edited and copied to `.env`).

The backend parses the following configuration variables:
```
CUNEI_PORT        # The port that the backend is listening on (default: 3000)
CUNEI_REDIS_HOST  # The host for the Redis instance (default: localhost)
CUNEI_REDIS_PORT  # The port for Redis (default: 6379)
CUNEI_METRICS_PORT # Port to bind prometheus metrics to (default: 9395)
```
The worker parses the following configuration variables:
```
CUNEI_REDIS_HOST   # The host for the Redis instance (default: localhost)
CUNEI_REDIS_PORT   # The port for Redis (default: 6379)
CUNEI_WORKER_QUEUE # The ID for the default queue the worker is processing (default: 0)
CUNEI_ZAMMAD_URL   # The URL for the Zammad instance's API
CUNEI_ZAMMAD_TOKEN # The authentication token for the Zammad instance
CUNEI_METRICS_PORT # Port to bind prometheus metrics to (default: 9396)
```
Additionally, we use the semi-standard `NODE_ENV` variable for defining test, staging, and production environments as well as [llog](https://github.com/mateodelnorte/llog) for setting logging verbosity.

## Deployment

### Standalone

First, clone this repository and from the root of the resulting directory install Cunei's dependencies:
```
npm install
```
Then, build all three components:
```
npm run build
```

And start the running processes (with necessary environment variables defined in `.env`):
```
npm run start
```

Additionally, components can be built or started individually using for example `npm run build:backend`, `npm run start:worker`, etc.

## API
The backend exposes the following HTTP API:

| Endpoint          | Method | Returns                                 | Implemented?       |
| :------:          | :----: | :-----:                                 | :----------:       |
| `/api/v1/reports` | `POST` | `{ report_id: '<UUID of new report>' }` | :heavy_check_mark: |


## License

[<img src="https://www.gnu.org/graphics/agplv3-155x51.png" alt="AGPLv3" >](http://www.gnu.org/licenses/agpl-3.0.html)

Cunei is a free software project licensed under the GNU Affero General Public License v3.0 (AGPLv3) by [Throneless Tech](https://throneless.tech).
